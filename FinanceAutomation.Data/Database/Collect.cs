//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinanceAutomation.Data.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Collect
    {
        public int Id { get; set; }
        public System.DateTime SalesDate { get; set; }
        public string Pnr { get; set; }
        public string IntDom { get; set; }
        public string AirwayFormNo { get; set; }
        public string TicketNo { get; set; }
        public decimal Pay { get; set; }
        public decimal SpaceTax { get; set; }
        public decimal FuelGap { get; set; }
        public decimal ServicePrice { get; set; }
        public decimal BuyServicePrice { get; set; }
        public string PayMethod { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string CartNo { get; set; }
        public string Racecourse1 { get; set; }
        public string Racecourse2 { get; set; }
        public string Racecourse3 { get; set; }
        public string Racecourse4 { get; set; }
        public string Racecourse5 { get; set; }
        public string Class1 { get; set; }
        public string Class2 { get; set; }
        public string Class3 { get; set; }
        public string Class4 { get; set; }
        public string FlightNumber1 { get; set; }
        public string FlightNumber2 { get; set; }
        public string FlightNumber3 { get; set; }
        public string FlightNumber4 { get; set; }
        public System.DateTime DepartureDate1 { get; set; }
        public System.DateTime DepartureDate2 { get; set; }
        public System.DateTime DepartureDate3 { get; set; }
        public System.DateTime DepartureDate4 { get; set; }
        public string AirwayCode { get; set; }
        public string TicketCutter { get; set; }
        public int InfoId { get; set; }
    
        public virtual CompanyInfo CompanyInfo { get; set; }
    }
}
