USE [CollectDatabase]
GO
/****** Object:  Table [dbo].[AccountDetail]    Script Date: 10.09.2017 13:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDetail](
	[DetailId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyCode] [nvarchar](50) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[MersisNo] [nvarchar](10) NULL,
	[Address1] [nvarchar](150) NOT NULL,
	[Address2] [nvarchar](150) NULL,
	[District] [nvarchar](20) NOT NULL,
	[City] [nvarchar](20) NOT NULL,
	[PostCode] [nvarchar](5) NULL,
	[Telephone] [nvarchar](20) NOT NULL,
	[FaxNo] [nvarchar](20) NULL,
	[EMail] [nvarchar](20) NULL,
	[WebAddress] [nvarchar](30) NULL,
	[Commissioned] [nvarchar](20) NOT NULL,
	[ProcessWaitTime] [int] NOT NULL,
	[CreditLimit] [float] NOT NULL,
	[DueDate] [nvarchar](10) NOT NULL,
	[TCIdentificationNo] [nvarchar](11) NOT NULL,
	[TaxAccountNo] [nvarchar](50) NOT NULL,
	[TaxCircleName] [nvarchar](50) NOT NULL,
	[CommercialRegistryNo] [nvarchar](20) NOT NULL,
	[ProfessionRoom] [nvarchar](20) NOT NULL,
	[RoomRegistrationNo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_AccountDetail] PRIMARY KEY CLUSTERED 
(
	[DetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountPlan]    Script Date: 10.09.2017 13:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountPlan](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[AccountCode] [varchar](4) NOT NULL,
	[AccountName] [varchar](150) NOT NULL,
	[AccountType] [varchar](10) NOT NULL,
	[ParentAccountCode] [varchar](4) NULL,
	[IsSubAccount] [bit] NOT NULL,
	[IsParentAccount] [bit] NOT NULL,
	[ChildAccountCount] [int] NOT NULL,
	[FullAccountCode] [nvarchar](50) NOT NULL,
	[ParentAccountId] [int] NOT NULL,
	[DetailId] [int] NULL,
 CONSTRAINT [PK_AccountPlan] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Collect]    Script Date: 10.09.2017 13:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Collect](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesDate] [date] NOT NULL,
	[Pnr] [varchar](6) NOT NULL,
	[IntDom] [varchar](10) NOT NULL,
	[AirwayFormNo] [varchar](10) NOT NULL,
	[TicketNo] [varchar](10) NOT NULL,
	[Pay] [decimal](18, 2) NOT NULL,
	[SpaceTax] [decimal](18, 2) NOT NULL,
	[FuelGap] [decimal](18, 2) NOT NULL,
	[ServicePrice] [decimal](18, 2) NOT NULL,
	[BuyServicePrice] [decimal](18, 2) NOT NULL,
	[PayMethod] [varchar](10) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Surname] [varchar](20) NOT NULL,
	[CartNo] [varchar](30) NOT NULL,
	[Racecourse1] [varchar](10) NOT NULL,
	[Racecourse2] [varchar](10) NOT NULL,
	[Racecourse3] [varchar](10) NOT NULL,
	[Racecourse4] [varchar](10) NOT NULL,
	[Racecourse5] [varchar](10) NOT NULL,
	[Class1] [varchar](10) NOT NULL,
	[Class2] [varchar](10) NOT NULL,
	[Class3] [varchar](10) NOT NULL,
	[Class4] [varchar](10) NOT NULL,
	[FlightNumber1] [varchar](20) NOT NULL,
	[FlightNumber2] [varchar](20) NOT NULL,
	[FlightNumber3] [varchar](20) NOT NULL,
	[FlightNumber4] [varchar](20) NOT NULL,
	[DepartureDate1] [smalldatetime] NOT NULL,
	[DepartureDate2] [smalldatetime] NOT NULL,
	[DepartureDate3] [smalldatetime] NOT NULL,
	[DepartureDate4] [smalldatetime] NOT NULL,
	[AirwayCode] [varchar](20) NOT NULL,
	[TicketCutter] [varchar](50) NOT NULL,
	[InfoId] [int] NOT NULL,
 CONSTRAINT [PK_Collect_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 10.09.2017 13:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](20) NOT NULL,
	[InfoId] [int] NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyInfo]    Script Date: 10.09.2017 13:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfo](
	[InfoId] [int] IDENTITY(1,1) NOT NULL,
	[SalesDate] [varchar](10) NOT NULL,
	[Pnr] [varchar](10) NOT NULL,
	[IntDom] [varchar](10) NOT NULL,
	[AirwayFormNo] [varchar](30) NOT NULL,
	[TicketNo] [varchar](20) NOT NULL,
	[Pay] [varchar](10) NOT NULL,
	[SpaceTax] [varchar](20) NOT NULL,
	[FuelGap] [varchar](20) NOT NULL,
	[ServicePrice] [varchar](20) NOT NULL,
	[BuyServicePrice] [varchar](30) NOT NULL,
	[PayMethod] [varchar](20) NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[Surname] [varchar](10) NOT NULL,
	[CartNo] [varchar](20) NOT NULL,
	[Racecourse1] [varchar](20) NOT NULL,
	[Racecourse2] [varchar](20) NOT NULL,
	[Racecourse3] [varchar](20) NOT NULL,
	[Racecourse4] [varchar](20) NOT NULL,
	[Racecourse5] [varchar](20) NOT NULL,
	[Class1] [varchar](10) NOT NULL,
	[Class2] [varchar](10) NOT NULL,
	[Class3] [varchar](10) NOT NULL,
	[Class4] [varchar](10) NOT NULL,
	[FlightNumber1] [varchar](20) NOT NULL,
	[FlightNumber2] [varchar](20) NOT NULL,
	[FlightNumber3] [varchar](20) NOT NULL,
	[FlightNumber4] [varchar](20) NOT NULL,
	[DepartureDate1] [varchar](20) NOT NULL,
	[DepartureDate2] [varchar](20) NOT NULL,
	[DepartureDate3] [varchar](20) NOT NULL,
	[DepartureDate4] [varchar](20) NOT NULL,
	[AirwayCode] [varchar](20) NOT NULL,
	[TicketCutter] [varchar](20) NOT NULL,
 CONSTRAINT [PK_CompanyInfo] PRIMARY KEY CLUSTERED 
(
	[InfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AccountDetail] ON 

INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (1, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (2, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (3, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (4, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (5, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (6, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
INSERT [dbo].[AccountDetail] ([DetailId], [CompanyCode], [CompanyName], [MersisNo], [Address1], [Address2], [District], [City], [PostCode], [Telephone], [FaxNo], [EMail], [WebAddress], [Commissioned], [ProcessWaitTime], [CreditLimit], [DueDate], [TCIdentificationNo], [TaxAccountNo], [TaxCircleName], [CommercialRegistryNo], [ProfessionRoom], [RoomRegistrationNo]) VALUES (7, N'323', N'denme', N'1563', N'sanane', NULL, N'pendik', N'ist', N'96558', N'1234568798', N'1236589674', N'kdmeodjweld', N'dkende', N'kdm3ld', 125, 528, N'545454', N'8796445121', N'7945k', N'8796n', N'ojlm', N'kljhpo', N'kıljk')
SET IDENTITY_INSERT [dbo].[AccountDetail] OFF
SET IDENTITY_INSERT [dbo].[AccountPlan] ON 

INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (3, N'120', N'Alıcılar', N'Kebir', N'-1', 1, 0, 1, N'100', -1, 1)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (4, N'00', N'Fabrikalar', N'Kebir', N'120', 1, 1, 3, N'120 00', 3, 2)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (6, N'001', N'Toyato Otomativ', N'Kebir', N'00', 0, 1, 0, N'120 00 001', 4, 3)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (8, N'002', N'Hyundai', N'Kebir', N'00', 0, 1, 0, N'120 00 002', 4, 4)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (9, N'003', N'Kolin İnşaat', N'Kebir', N'00', 0, 1, 0, N'120 00 003', 4, 5)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (10, N'004', N'Tekno Ambalaj Sanayi', N'Kebir', N'00', 0, 1, 0, N'120 00 004', 4, 6)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (11, N'00', N'deneme12', N'Muavin', N'120', 0, 1, 0, N'120 00 005', 4, 7)
INSERT [dbo].[AccountPlan] ([AccountId], [AccountCode], [AccountName], [AccountType], [ParentAccountCode], [IsSubAccount], [IsParentAccount], [ChildAccountCount], [FullAccountCode], [ParentAccountId], [DetailId]) VALUES (13, N'130', N'try', N'Grup', N'100', 0, 1, 0, N'100 130', 3, 1)
SET IDENTITY_INSERT [dbo].[AccountPlan] OFF
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([CompanyId], [CompanyName], [InfoId]) VALUES (1, N'Biletbank', 1)
INSERT [dbo].[Companies] ([CompanyId], [CompanyName], [InfoId]) VALUES (2, N'Pegasus', 2)
INSERT [dbo].[Companies] ([CompanyId], [CompanyName], [InfoId]) VALUES (3, N'GTC(Tatil Budur)', 3)
INSERT [dbo].[Companies] ([CompanyId], [CompanyName], [InfoId]) VALUES (4, N'Iati', 4)
INSERT [dbo].[Companies] ([CompanyId], [CompanyName], [InfoId]) VALUES (5, N'Booking Agora', 5)
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[CompanyInfo] ON 

INSERT [dbo].[CompanyInfo] ([InfoId], [SalesDate], [Pnr], [IntDom], [AirwayFormNo], [TicketNo], [Pay], [SpaceTax], [FuelGap], [ServicePrice], [BuyServicePrice], [PayMethod], [Name], [Surname], [CartNo], [Racecourse1], [Racecourse2], [Racecourse3], [Racecourse4], [Racecourse5], [Class1], [Class2], [Class3], [Class4], [FlightNumber1], [FlightNumber2], [FlightNumber3], [FlightNumber4], [DepartureDate1], [DepartureDate2], [DepartureDate3], [DepartureDate4], [AirwayCode], [TicketCutter]) VALUES (1, N'Tarih', N'Pnr', N'IntDom', N'Hava Yolu Form No', N'Bilet No', N'Ücret', N'Vergi', N'Surcharge', N'Komisyon', N'SC', N'Ödeme Şekli', N'Adı', N'Soyadı', N'Kart No', N'Parkur1', N'Parkur2', N'Parkur3', N'Parkur4', N'Parkur5', N'Sınıf1', N'Sınıf2', N'Sınıf3', N'Sınıf4', N'Uçuş No1', N'Uçuş No2', N'Uçuş No3', N'Uçuş No4', N'KalkisTarihi1', N'KalkisTarihi2', N'KalkisTarihi3', N'KalkisTarihi4', N'Hava Yolu Kodu', N'Kesen Kişi')
INSERT [dbo].[CompanyInfo] ([InfoId], [SalesDate], [Pnr], [IntDom], [AirwayFormNo], [TicketNo], [Pay], [SpaceTax], [FuelGap], [ServicePrice], [BuyServicePrice], [PayMethod], [Name], [Surname], [CartNo], [Racecourse1], [Racecourse2], [Racecourse3], [Racecourse4], [Racecourse5], [Class1], [Class2], [Class3], [Class4], [FlightNumber1], [FlightNumber2], [FlightNumber3], [FlightNumber4], [DepartureDate1], [DepartureDate2], [DepartureDate3], [DepartureDate4], [AirwayCode], [TicketCutter]) VALUES (2, N'Tarih', N'PNR', N'Int-Dom', N'AirwayFormNo', N'Bilet No', N'Ücret', N'Vergi', N'FuelGap', N'Komisyon', N'', N'Odeme Sekli', N'Adı', N'Soyadı', N'Kart Numarasi', N'Racecourse1', N'Racecourse2', N'Racecourse3', N'Racecourse4', N'Racecourse5', N'Class1', N'Class2', N'Class3', N'Class4', N'FlightNumber1', N'FilghtNumber2', N'FlightNumber3', N'FlightNumber4', N'DepartureDate1', N'DepartureDate2', N'DepartureDate3', N'DepartureDate4', N'AirwayCode', N'KULLANICI ADI')
INSERT [dbo].[CompanyInfo] ([InfoId], [SalesDate], [Pnr], [IntDom], [AirwayFormNo], [TicketNo], [Pay], [SpaceTax], [FuelGap], [ServicePrice], [BuyServicePrice], [PayMethod], [Name], [Surname], [CartNo], [Racecourse1], [Racecourse2], [Racecourse3], [Racecourse4], [Racecourse5], [Class1], [Class2], [Class3], [Class4], [FlightNumber1], [FlightNumber2], [FlightNumber3], [FlightNumber4], [DepartureDate1], [DepartureDate2], [DepartureDate3], [DepartureDate4], [AirwayCode], [TicketCutter]) VALUES (3, N'Tarih', N'Pnr', N'Int-Dom', N'AirwayFormNo', N'Bilet No', N'Ücret', N'Vergi', N'Surcharge', N'HİZMET TUTARI', N'KOMISYON', N'Odeme Sekli', N'Adı', N'Soyadı', N'Kart Numarasi', N'Parkur1', N'Parkur2', N'Parkur3', N'Parkur4', N'Parkur5', N'Class1', N'Class2', N'Class3', N'Class4', N'', N'', N'', N'', N'Gidiş Tarihi', N'Dönüş Tarihi', N'', N'', N'HavaYolu Kodu', N'Kesen Kişi')
INSERT [dbo].[CompanyInfo] ([InfoId], [SalesDate], [Pnr], [IntDom], [AirwayFormNo], [TicketNo], [Pay], [SpaceTax], [FuelGap], [ServicePrice], [BuyServicePrice], [PayMethod], [Name], [Surname], [CartNo], [Racecourse1], [Racecourse2], [Racecourse3], [Racecourse4], [Racecourse5], [Class1], [Class2], [Class3], [Class4], [FlightNumber1], [FlightNumber2], [FlightNumber3], [FlightNumber4], [DepartureDate1], [DepartureDate2], [DepartureDate3], [DepartureDate4], [AirwayCode], [TicketCutter]) VALUES (4, N'tarih', N'pnr', N'int/dom', N'havayolu kodu', N'e-bilet', N'fiyat', N'havalimanı vergisi', N'Vergiler', N'komisyon', N'sc', N'ödeme', N'isim', N'soyisim', N'k. kart no', N'parkur 1', N'parkur 2', N'parkur 3', N'parkur 4', N'parkur 5', N'sınıf 1', N'sınıf 2', N'sınıf 3', N'sınıf 4', N'Uçuş Numarası 1', N'Uçuş Numarası 2', N'Uçuş Numarası 3', N'Uçuş Numarası 4', N'kalkış tarihi 1', N'kalkış tarihi 2', N'kalkış tarihi 3', N'kalkış tarihi 4', N'AirwayCode', N'acente kullanıcısı')
INSERT [dbo].[CompanyInfo] ([InfoId], [SalesDate], [Pnr], [IntDom], [AirwayFormNo], [TicketNo], [Pay], [SpaceTax], [FuelGap], [ServicePrice], [BuyServicePrice], [PayMethod], [Name], [Surname], [CartNo], [Racecourse1], [Racecourse2], [Racecourse3], [Racecourse4], [Racecourse5], [Class1], [Class2], [Class3], [Class4], [FlightNumber1], [FlightNumber2], [FlightNumber3], [FlightNumber4], [DepartureDate1], [DepartureDate2], [DepartureDate3], [DepartureDate4], [AirwayCode], [TicketCutter]) VALUES (5, N'Tarih', N'Pnr', N'Int-Dom', N'Bilet No', N'Ucret', N'Vergi', N'Vergi', N'Surcharge', N'Hizmet Tutarı', N'Komisyon', N'Odeme Sekli', N'Adı', N'Soyadı', N'Kart Numarası', N'Parkur1', N'Parkur2', N'Parkur3', N'Parkur4', N'Parkur5', N'Class1', N'Class2', N'Class3', N'Class4', N'', N'', N'', N'', N'Gidiş Tarihi', N'Dönüş Tarihi', N'', N'', N'HavaYolu Kodu', N'')
SET IDENTITY_INSERT [dbo].[CompanyInfo] OFF
ALTER TABLE [dbo].[AccountPlan]  WITH CHECK ADD  CONSTRAINT [FK_AccountPlan_AccountDetail] FOREIGN KEY([DetailId])
REFERENCES [dbo].[AccountDetail] ([DetailId])
GO
ALTER TABLE [dbo].[AccountPlan] CHECK CONSTRAINT [FK_AccountPlan_AccountDetail]
GO
ALTER TABLE [dbo].[Collect]  WITH CHECK ADD  CONSTRAINT [FK_Collect_CompanyInfo] FOREIGN KEY([InfoId])
REFERENCES [dbo].[CompanyInfo] ([InfoId])
GO
ALTER TABLE [dbo].[Collect] CHECK CONSTRAINT [FK_Collect_CompanyInfo]
GO
ALTER TABLE [dbo].[Companies]  WITH CHECK ADD  CONSTRAINT [FK_Companies_CompanyInfo] FOREIGN KEY([InfoId])
REFERENCES [dbo].[CompanyInfo] ([InfoId])
GO
ALTER TABLE [dbo].[Companies] CHECK CONSTRAINT [FK_Companies_CompanyInfo]
GO
