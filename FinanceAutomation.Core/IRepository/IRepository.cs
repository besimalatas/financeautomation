﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinanceAutomation.Core.IRepository
{
    internal interface IRepository<T> where T : class
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj">Ekleme veya guncelleme işlemini gerçekleştiren metod obje gonderiniz</param>
        /// <returns></returns>
        void AddOrUpdate(T obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"> id ye gore tek item donduren method</param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// ilgili repository'deki butun dataları getiren metod
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression">Where sartı ile sonuc donduren metod: metoda kosul gonderiniz</param>
        /// <returns></returns>
        List<T> GetMany(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression">Saglanan where sartına göre Tekil obje geri donduren metod</param>
        /// <returns></returns>
        T Get(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Değişiklikleri Kaydet
        /// </summary>
        void Save();

    }
}
