﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FinanceAutomation.Data.Database;
using FinanceAutomation.Core.IRepository;

namespace FinanceAutomation.Core.Repository
{
    public class AccountDetailRepository : IRepository<AccountDetail>
    {
        private readonly CollectDatabaseEntities _entities;

        public AccountDetailRepository()
        {
            _entities = new CollectDatabaseEntities();
        }

        public void AddOrUpdate(AccountDetail obj)
        {

            _entities.AccountDetail.AddOrUpdate(obj);
        }

        public AccountDetail GetById(int id)
        {
            return _entities.AccountDetail.FirstOrDefault(x => x.AccountId == id);
        }

        public List<AccountDetail> GetAll()
        {
            return _entities.AccountDetail.ToList();
        }

        public List<AccountDetail> GetMany(Expression<Func<AccountDetail, bool>> expression)
        {
            return _entities.AccountDetail.Where(expression).ToList();
        }

        public AccountDetail Get(Expression<Func<AccountDetail, bool>> expression)
        {
            return _entities.AccountDetail.FirstOrDefault(expression);
        }

        public void Save()
        {
            _entities.SaveChanges();
        }
    }
}
