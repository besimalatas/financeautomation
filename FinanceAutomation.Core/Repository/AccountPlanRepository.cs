﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FinanceAutomation.Data.Database;
using FinanceAutomation.Core.IRepository;

namespace FinanceAutomation.Core.Repository
{
    public class AccountPlanRepository : IRepository<AccountPlan>
    {
        private readonly CollectDatabaseEntities _entities;

        public AccountPlanRepository()
        {
            _entities = new CollectDatabaseEntities();
        }

        public void AddOrUpdate(AccountPlan obj)
        {
            _entities.AccountPlan.AddOrUpdate(obj);

        }

        public AccountPlan GetById(int id)
        {
            return _entities.AccountPlan.FirstOrDefault(x => x.AccountId == id);
        }

        public List<AccountPlan> GetAll()
        {
            return _entities.AccountPlan.ToList();
        }

        public List<AccountPlan> GetMany(Expression<Func<AccountPlan, bool>> expression)
        {
            return _entities.AccountPlan.Where(expression).ToList();
        }

        public AccountPlan Get(Expression<Func<AccountPlan, bool>> expression)
        {
            return _entities.AccountPlan.FirstOrDefault(expression);
        }

        public void Save()
        {
            _entities.SaveChanges();
        }
    }
}
