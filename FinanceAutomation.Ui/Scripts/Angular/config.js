﻿
var application = angular.module("application", ['ui.router', 'cp.ngConfirm']);

application.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('load',
        {
            url: '/loadExcel',
            templateUrl: ' ../Templates/Load.html'
        
        })
       .state('account',
        {
            url: '/account',
            templateUrl: ' ../Templates/Account.html'
            
        })
          .state('account.newAccount',
        {
            url: 'as',
            templateUrl: ' ../Templates/Add.html'

        })
     .state('newAccount',
        {
            url: '/add',
            templateUrl: ' ../Templates/Add.html'

        });

});
