﻿application.factory('alert',
    function ($ngConfirm) {

        var service = {};
    
        service.error = function (message) {
            $ngConfirm({
                title: 'HATA!',
                type: 'red',
                content: message,
                buttons: {
                    confirm: {
                        text: 'Tamam',
                        keys: ['enter']
                    }
                }
            });
        }
        service.saved = function() {
            $ngConfirm({
                title: 'Bilgi',
                type: 'green',
                content: 'Kaydetme işlemi başarılı',
                buttons: {
                    confirm: {
                        text: 'Tamam',
                        keys: ['enter'],
                        btnClass: 'btn-primary'
                    }
                }
            });
        };

        service.success = function() {
            $ngConfirm({
                title: 'Bilgi',
                type: 'green',
                content: 'İşlem başarılı',
                buttons: {
                    confirm: {
                        text: 'Tamam',
                        keys: ['enter']
                    }
                }
            });
        };

        service.description = function(message) {
            $ngConfirm({
                title: 'Açıklama',
                type: 'blue',
                content: message,
                buttons: {
                    confirm: {
                        text: 'Tamam',
                        keys: ['enter', 'esc', 'space']
                    }
                }
            });
        };


        return service;
    });