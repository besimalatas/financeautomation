application.factory('rest',
    function ($http, $rootScope) {
        var service = {};

        service.get = function (url, data) {
            return service.execute("GET", url, data, callback);
        };

        service.post = function (url, data, callback) {
            return service.execute("POST", url, data, callback);
        };

        service.execute = function (method, url, data, callback) {

            $http({
                method: method,
                url: url,
                params: data,
                headers: { 'Accept': 'application/json' }

            }).then(function (response) {
                if (typeof callback === "function") {
                    callback(response);
                }
                $rootScope.$broadcast("Request.Off");
            }, function (response) {
                $rootScope.$broadcast("Request.Off");

            });

        }
        service.success = function (response) {
            return response;
        }


        return service;
    });