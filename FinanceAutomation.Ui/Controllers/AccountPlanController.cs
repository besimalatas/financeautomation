﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using FinanceAutomation.Data.Database;
using System.IO;
using FinanceAutomation.Core.Repository;


namespace FinanceAutomation.Ui.Controllers
{
    public class AccountPlanController : Controller
    {

        [HttpGet]
        [ActionName("GetAllAccounts")]
        public JsonResult GetAccount()
        {
            AccountPlanRepository plan = new AccountPlanRepository();

            return Json(plan.GetAll().Select(x => new AccountPlan
            {
                AccountType = x.AccountType,
                AccountId = x.AccountId,
                AccountName = x.AccountName,
                AccountCode = x.AccountCode,
                FullAccountCode = x.FullAccountCode

            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("GetAccounts")]
        public JsonResult GetAccount(AccountPlan accountPlan)
        {
            AccountPlanRepository plan = new AccountPlanRepository();

            return Json(plan.GetMany(x => x.ParentAccountId == accountPlan.ParentAccountId).Select(x => new AccountPlan
            {
                AccountType = x.AccountType,
                AccountId = x.AccountId,
                AccountName = x.AccountName,
                AccountCode = x.AccountCode,
                FullAccountCode = x.FullAccountCode

            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("AddAccount")]
        public JsonResult AddAccount(AccountPlan accountPlan)
        {


            AccountPlanRepository repository = new AccountPlanRepository();
            repository.AddOrUpdate(accountPlan);
            repository.Save();

            return Json(true, JsonRequestBehavior.AllowGet);
             
          
        }
    }
}