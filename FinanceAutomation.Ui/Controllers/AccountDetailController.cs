﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using FinanceAutomation.Data.Database;
using System.IO;
using System.Web.Http;
using FinanceAutomation.Core.Repository;

namespace FinanceAutomation.Ui.Controllers
{
    public class AccountDetailController : Controller
    {

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.ActionName("GetDetails")]
        public JsonResult GetDetails(AccountDetail accountDetail)
        {

            AccountDetailRepository detail = new AccountDetailRepository();

            return Json(detail.GetMany(x => x.AccountId == accountDetail.AccountId).Select(x => new AccountDetail
            {
                Address1 = x.Address1,
                Address2 = x.Address2,
                City = x.City,
                CommercialRegistryNo = x.CommercialRegistryNo,
                Commissioned = x.Commissioned,
                CompanyCode = x.CompanyCode,
                CompanyName = x.CompanyName,
                CreditLimit = x.CreditLimit,
                DetailId = x.DetailId,
                District = x.District,
                DueDate = x.DueDate,
                EMail = x.EMail,
                FaxNo = x.FaxNo,
                MersisNo = x.MersisNo,
                PostCode = x.PostCode,
                ProcessWaitTime = x.ProcessWaitTime,
                ProfessionRoom = x.ProfessionRoom,
                RoomRegistrationNo = x.RoomRegistrationNo,
                TCIdentificationNo = x.TCIdentificationNo,
                TaxAccountNo = x.TaxAccountNo,
                TaxCircleName = x.TaxCircleName,
                Telephone = x.Telephone,
                WebAddress = x.WebAddress
            }), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.ActionName("AddDetails")]
        public JsonResult AddDetails([FromBody]AccountDetail accountDetail)
        {

            AccountDetailRepository repository = new AccountDetailRepository();

            var result = repository.Get(x => x.AccountId ==accountDetail.AccountId);
            var accountDetailId = 0;

            if(result!=null)

            {
                accountDetailId = result.DetailId;
            }

            AccountDetail _accountDetail=new AccountDetail();

            _accountDetail = accountDetail;
            _accountDetail.DetailId = accountDetailId;

            repository.AddOrUpdate(accountDetail);
            repository.Save();

            return Json(true, JsonRequestBehavior.AllowGet);
        }

       
    }

}