﻿application.controller("addController",["$scope", "$http", "$rootScope", "rest", "alert",
       function ($scope, $http, $rootScope, rest, alert) {

           $scope.dataSet = {
               data: [],               // db'dan donen result'ların tutulacagi array                
               currentAccount: {       // selecten secilen item'ın tutulacagi obje
               },
               newAccount: {           // yeni hesap bilgilerinin tutulacagi obje

               },
               accountTypes: [         // hesap tipleri listesi
                   {
                       name: "Kebir",
                       code: 1
                   },
               {
                   name: "Muavin",
                   code: 2
               },
               {
                   name: "Grup",
                   code: 3
               }
               ],
               walkingInAccounts: {     // burası sonradan refactor edilmeli, çol karışık. hesaplar arasi gezinti icin obje
                   walkingInAccountsArray: [-1],   // walkingInAccountsArray: secilen objenin AccountId'lerinin tutulacağı liste.açılan her yeni bir alt  hesapta bu arraya o hesabın AccountId'si ekleniyor ve bir ust hesaba donuldugunde siliniyor. boylece gezinilen hesaplar arasinda baglanti kopmamis oluyor
                   walkingAccountsArray: [] //  bir alt veya ust hesaba gecilmesi durumunda bu array'da surekli guncellenerek en bastan en sona kadar gezilen butun hesaplar tutulmus oluyor
               },

               stepNumber: 0,  // gezinilen adım numarası, toplam kac alt hesaba dallanılmis bilgisi
               lastSelectedAccount: {} // son secilen Account bilgisi
           };

           rest.post("/AccountPlan/GetAccounts", { ParentAccountId: -1 }
             , function (json) {
                 $scope.dataSet.data = json.data;
                 $scope.dataSet.data.unshift({
                     AccountId: -1,
                     AccountCode: "Seçiniz",
                     AccountName: "",
                     AccountType: "",
                     PArentAccountCode: "",
                     IsSubAccount: false,
                     IsParentAccount: false,
                     ChildAccountCount: 0,
                     FullAccountCode: "Seçiniz",
                     ParentAccountCode: ""
                 });
                 $scope.dataSet.currentAccount = $scope.dataSet.data[0];
             });


           /**
            * / hesap secilmesi durumunda secilen hesaba ait alt hesap
            * bilgilerinin getirilerek selectin guncellenmesi veya
            * ust hesaplara donulmesi islemleri
            * @returns {} 
            */
           $scope.selectAccount = function () {
               /**
                * /request icin query'
                */
               var query = {
               }

               /*eger secilen option ust hesaba don ise 
               */
               if ($scope.dataSet.currentAccount.AccountCode === "Üst Hesaplara Geri Dön") {
                   // bir ust hesaba donulecegi icin array'lara eklenen son AccountId siliniyor
                   $scope.dataSet.walkingInAccounts.walkingInAccountsArray.splice($scope.dataSet.stepNumber, 1);
                   // bir ust ehsaba donulecegi icin array'lara eklenen son hesap siliniyor
                   $scope.dataSet.walkingInAccounts.walkingAccountsArray.splice($scope.dataSet.stepNumber - 1, 1);
                   $scope.dataSet.stepNumber += -1;
                   // son hesap silindikten sonra bir onceki hesap bizim icin son hesap olmakta, yani bir ust hesaba mantıken donmus olduk
                   // quert'imizde bu hesap 'ın parentAccountId's, üzerinden istek atarak selecti guncelliyoruz
                   query = {
                       method: 'POST',
                       url: '/AccountPlan/GetAccounts',
                       data: {
                           ParentAccountId: $scope.dataSet.walkingInAccounts.walkingInAccountsArray[$scope.dataSet.stepNumber]
                       }
                   }
               } else {
                   // yeni bir dallanma yapılarak yeni alt hesapların bilgileri getirilecekse
                   // yeni secilen hesabın account ıd'si arraya ekleniyor
                   $scope.dataSet.walkingInAccounts.walkingInAccountsArray.push($scope.dataSet.currentAccount.AccountId);
                   // yeni secilen hesabın account arraya ekleniyor
                   $scope.dataSet.walkingInAccounts.walkingAccountsArray.push($scope.dataSet.currentAccount);
                   $scope.dataSet.stepNumber += 1;
                   query = {
                       method: 'POST',
                       url: '/AccountPlan/GetAccounts',
                       data: {
                           ParentAccountId: $scope.dataSet.currentAccount.AccountId
                       }
                   }
               }
               //eger en ust hesaplara kadar(ana hesaplara) geri donulduyse yeni hesap bilgisi icin gerekli olan hesap kodu "" setleniyor
               if ($scope.dataSet.stepNumber === 0) {
                   $scope.dataSet.newAccount
                       .accountCode = "";
               }
                   // bir ust veya yeni alt hesaplar icin istekle bulunuldugu icin yeni hesapkodu secilen son hesap uzerinden guncelleniyor
               else {
                   $scope.dataSet.newAccount
                .accountCode = $scope.dataSet.walkingInAccounts
                .walkingAccountsArray[$scope.dataSet.stepNumber - 1].FullAccountCode + " ";
               }
               rest.post("/AccountPlan/GetAccounts",
                                   query.data,
                                   function (json) {
                                       $scope.dataSet.data = json.data;
                                       // eger ana hesaplara kadar geri donulduyse seciniz en basa eklenerek senaryo bastan kuruluyor
                                       if ($scope.dataSet.data.length > 0 && $scope.dataSet.data[0].ParentAccountId === -1) {
                                           $scope.dataSet.data.unshift({
                                               AccountId: -1,
                                               AccountCode: "Seçiniz",
                                               AccountName: "",
                                               AccountType: "",
                                               PArentAccountCode: "",
                                               IsSubAccount: false,
                                               IsParentAccount: false,
                                               ChildAccountCount: 0,
                                               FullAccountCode: "Seçiniz",
                                               ParentAccountCode: "",

                                           });
                                           $scope.dataSet.currentAccount = $scope.dataSet.data[0];
                                       } else {
                                           /* eger ust hesaplara geri don secilmediyse gelen alt hesapları bilgilerinin onu secilen hesap ekleniyor*/
                                           if ($scope.dataSet.currentAccount.AccountCode !== "Üst Hesaplara Geri Dön") {
                                               $scope.dataSet.data.unshift($scope.dataSet.currentAccount);
                                           } else {
                                               $scope.dataSet.data.unshift(
                                                   $scope.dataSet.walkingInAccounts.walkingAccountsArray[$scope.dataSet.stepNumber - 1]);
                                           }
                                           /**
                                            * / ust hesaplara geri don seceneği her harikularda ekleniyor(ana hesaplara kadar geri donulmediyse)
                                            */
                                           $scope.dataSet.data.unshift({
                                               AccountId: $scope.dataSet.currentAccount.AccountId,
                                               AccountCode: "Üst Hesaplara Geri Dön",
                                               AccountName: "",
                                               AccountType: "",
                                               PArentAccountCode: "",
                                               IsSubAccount: false,
                                               IsParentAccount: false,
                                               ChildAccountCount: 0,
                                               FullAccountCode: "Üst Hesaplara Geri Dön",
                                               ParentAccountCode: "",

                                               ParentAccountId: $scope.dataSet.currentAccount.ParentAccountId
                                           });

                                           $scope.dataSet.currentAccount = $scope.dataSet.data[1];
                                       }
                                   });
           };
           /**
            * yeni hesap kodu icin input surekli kontrol ediliyor
            * alt hesaba dallanıydıysa yeni hesap kodu <parent hesap kodu>+ yeni hesap kodu seklinde olmalı
            * kullanııc parentdan gerisini silemez 
            * eger sildiyse parent hesap kodu yeniden inputa setlenir
            * angular harika !
            */
           $scope.$watch('dataSet.newAccount.accountCode',
                function () {

                    if ($scope.dataSet.currentAccount.FullAccountCode !== undefined &&
                        $scope.dataSet.currentAccount.FullAccountCode !== "Üst Hesaplara Geri Dön" &&
                        $scope.dataSet.currentAccount.FullAccountCode !== "Seçiniz") {
                        if ($scope.dataSet.newAccount.accountCode.length - 1 <
                      $scope.dataSet.currentAccount.FullAccountCode.length) {
                            $scope.dataSet.newAccount.accountCode = $scope.dataSet.currentAccount.FullAccountCode + " ";
                        }
                    }

                });

           $scope.save = function() {
               $rootScope.$broadcast("Request.On");

               if ($scope.dataSet.newAccount.accountCode === undefined ||
                       $scope.dataSet.newAccount.accountName === undefined ||
                       $scope.dataSet.newAccount.accountType.name === undefined
               ) {
                   return alert.error("Boş Alan Bırakılamaz");
               };

               $http({
                       method: 'POST',
                       url: '/AccountPlan/AddAccount',
                       data: {
                           FullAccountCode: $scope.dataSet.newAccount.accountCode,
                           AccountCode: $scope.dataSet.newAccount.accountCode
                               .split(" ")[$scope.dataSet.newAccount.accountCode.split.length - 1],
                           AccountName: $scope.dataSet.newAccount.accountName,
                           AccountType: $scope.dataSet.newAccount.accountType.name,
                           ParentAccountCode: $scope.dataSet.newAccount.accountCode
                               .split(" ")[$scope.dataSet.newAccount.accountCode.split.length - 2],
                           IsSubAccount: false,
                           IsParentAccount: $scope.dataSet.currentAccount.AccountId === -1 ? false : true,
                           ChildAccountCount: 0,
                           ParentAccountId: $scope.dataSet.currentAccount.AccountId

                       }

                   })
                   .then(function successCallback(response) {
                           $rootScope.$broadcast("Request.Off");
                           alert.success();
                       },
                       function errorCallback(response) {
                           $rootScope.$broadcast("Request.Off");
                           alert.error('Hata Mesajı');
                       });
           };
       }]);