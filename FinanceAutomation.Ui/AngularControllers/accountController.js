﻿application.controller("accountController", ["$scope", "$http", "$rootScope", "rest", "alert",
    function ($scope, $http, $rootScope, rest, alert) {

        $scope.dataSet = {
            data: [],
            dataOfPerPage: 10,
            start: 0,
            to: 10,
            totalPageNumber: 0,
            isDetail: false,
            selectedAccount: {},
            addAccountId: 0
        };



        $rootScope.$broadcast("Request.On");
        $http({
            method: 'GET',
            url: '/AccountPlan/GetAllAccounts'
        }).then(function successCallback(response) {
            $scope.dataSet.data = response.data;
            $scope.dataSet.totalPageNumber = Math.ceil($scope.dataSet.data.length / $scope.dataSet.dataOfPerPage);
            $rootScope.$broadcast("Request.Off");
        }, function errorCallback(response) {
        });



        $scope.getNumber = function (num) {
            return new Array(num);
        }

        $scope.changePage = function (pageNumber) {
            $scope.dataSet.start = $scope.dataSet.dataOfPerPage * pageNumber;
            $scope.dataSet.to = ($scope.dataSet.dataOfPerPage * pageNumber) + $scope.dataSet.dataOfPerPage;
        }

        $scope.tic = function (item) {

            $scope.dataSet.isDetail = true;
            $scope.dataSet.addAccountId = item.AccountId;
            rest.post("/AccountDetail/GetDetails", { AccountId: item.AccountId }, function (json) {
                if (json.data !== null || json.data !== undefined) {
                    $scope.dataSet.selectedAccount = json.data[0];
                }
            });

        };

        $scope.save = function () {
            $rootScope.$broadcast("Request.On");

            if ($scope.dataSet.selectedAccount.CompanyCode === undefined ||
                    $scope.dataSet.selectedAccount.CompanyName === undefined ||
                    $scope.dataSet.selectedAccount.MersisNo === undefined ||
                    $scope.dataSet.selectedAccount.Address1 === undefined ||
                    $scope.dataSet.selectedAccount.Address2 === undefined ||
                    $scope.dataSet.selectedAccount.District === undefined ||
                    $scope.dataSet.selectedAccount.City === undefined ||
                    $scope.dataSet.selectedAccount.PostCode === undefined ||
                    $scope.dataSet.selectedAccount.Telephone === undefined ||
                    $scope.dataSet.selectedAccount.FaxNo === undefined ||
                    $scope.dataSet.selectedAccount.EMail === undefined ||
                    $scope.dataSet.selectedAccount.WebAddress === undefined ||
                    $scope.dataSet.selectedAccount.Commissioned === undefined ||
                    $scope.dataSet.selectedAccount.ProcessWaitTime === undefined ||
                    $scope.dataSet.selectedAccount.CreditLimit === undefined ||
                    $scope.dataSet.selectedAccount.DueDate === undefined ||
                    $scope.dataSet.selectedAccount.TCIdentificationNo === undefined ||
                    $scope.dataSet.selectedAccount.TaxAccountNo === undefined ||
                    $scope.dataSet.selectedAccount.TaxCircleName === undefined ||
                    $scope.dataSet.selectedAccount.CommercialRegistryNo === undefined ||
                    $scope.dataSet.selectedAccount.ProfessionRoom === undefined ||
                    $scope.dataSet.selectedAccount.RoomRegistrationNo === undefined
            ) { return alert.error("Boş Alan Bırakılamaz"); };

            $http({
                method: 'POST',
                url: '/AccountDetail/AddDetails',
                data: {
                    AccountId: $scope.dataSet.addAccountId,
                    accountDetail: $scope.dataSet.selectedAccount
                   
                }
            })
                .then(function successCallback(response) {
                    $rootScope.$broadcast("Request.Off");
                    alert.success();
                }, function errorCallback(response) {
                    $rootScope.$broadcast("Request.Off");
                    alert.error('Hata Mesajı');
                });

        };

        //var query = {
        //    CompanyCode: $scope.dataSet.selectedAccount.CompanyCode,
        //    CompanyName: $scope.dataSet.selectedAccount.CompanyName,
        //    MersisNo: $scope.dataSet.selectedAccount.MersisNo,
        //    Address1: $scope.dataSet.selectedAccount.Address1,
        //    Address2: $scope.dataSet.selectedAccount.Address2,
        //    District: $scope.dataSet.selectedAccount.District,
        //    City: $scope.dataSet.selectedAccount.City,
        //    PostCode: $scope.dataSet.selectedAccount.PostCode,
        //    Telephone: $scope.dataSet.selectedAccount.Telephone,
        //    FaxNo: $scope.dataSet.selectedAccount.FaxNo,
        //    EMail: $scope.dataSet.selectedAccount.EMail,
        //    WebAddress: $scope.dataSet.selectedAccount.WebAddress,
        //    Commissioned: $scope.dataSet.selectedAccount.Commissioned,
        //    ProcessWaitTime: $scope.dataSet.selectedAccount.ProcessWaitTime,
        //    CreditLimit: $scope.dataSet.selectedAccount.CreditLimit,
        //    DuaDate: $scope.dataSet.selectedAccount.DuaDate,
        //    TCIdentificationNo: $scope.dataSet.selectedAccount.TCIdentificationNo,
        //    TaxAccountNo: $scope.dataSet.selectedAccount.TaxAccountNo,
        //    TaxCircleName: $scope.dataSet.selectedAccount.TaxCircleName,
        //    CommercialRegistryNo: $scope.dataSet.selectedAccount.CommercialRegistryNo,
        //    ProfessionRoom: $scope.dataSet.selectedAccount.ProfessionRoom,
        //    RoomRegistrationNo: $scope.dataSet.selectedAccount.RoomRegistrationNo
        //}
        //console.log(query);
        //rest.post("/AccountDetail/AddDetails",
        //{

        //   query

        //}, function (json) {

        //});


        $scope.exit = function () {
            $scope.dataSet.isDetail = false;
        }


    }]);