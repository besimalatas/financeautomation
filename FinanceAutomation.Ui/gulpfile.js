﻿var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	minifyCSS = require('gulp-minify-css'),
	concat = require('gulp-concat');


gulp.task('scripts', function () {
    gulp.src('app/js/*.js')
		.pipe(uglify())
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('dist'))
});

gulp.task('styles', function () {
    gulp.src('Content/*.css')
		.pipe(minifyCSS({ keepBreaks: true }))
		.pipe(gulp.dest('dist/css'))
});

gulp.task('watch', function () {
    gulp.watch('Scripts/*.js', ['scripts']);
    gulp.watch('Content/*.css', ['styles']);
});

gulp.task('default', ['scripts', 'styles']);